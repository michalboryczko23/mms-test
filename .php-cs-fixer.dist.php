<?php

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        '@PSR1' => true,
        '@PSR12' => true,
        'native_function_invocation' => [
            'exclude' => []
        ],
        'blank_line_before_statement' => ['statements' => ['return']],
    ])
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in(__DIR__.DIRECTORY_SEPARATOR.'src')
            ->in(__DIR__.DIRECTORY_SEPARATOR.'tests')
    )
    ->setRiskyAllowed(true)
    ->setUsingCache(true);

