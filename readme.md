# Mr & Mrs Smith test task - simple calculator

## How to run:

I used docker to make it easy to start and work with this application.
To start the app, you should exec below commands:

    docker-compose up -d
    docker-compose exec -u application php composer install

By default HTTP port is `6099`, but it is possible to change it in the `.env` file by changing the value of `HTTP_EXPOSED_PORT`.

## How to use:

The application provides REST API interface for users.

There are 2 available endpoints:

`GET /api/operations` - returns lists of supported operations

    {
		"operations":  ["+","/","*","-"]
	}

`POST /api/calculate` - do calculate. Required body:

    {
		"a":  11, - left side of operation
		"b":  0, - right side of operations
		"operator":  "+" - kind of calculation
	}

[Sample POSTMAN collection available here](https://www.getpostman.com/collections/60cb4b67e8e29061b62d)

## How to extend:

I used a strategy pattern to implement a calculator.

So if you want to add additional operations, you should create your implementation of `App\Core\Service\CalculateStrategy\CalculationStrategyInterface`.

Symfony di will inject your implementation to the calculator service automatically by using the `tagged_iterator`

## Tests:

I used phpunit for unit tests.

To run unit tests you should exec below command:

    vendor/bin/phpunit --testdox
