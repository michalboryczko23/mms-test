<?php

namespace App\Infrastructure\Service\Validation;

use App\Infrastructure\Adapter\Incoming\App\CalculatorAdapter;
use App\Infrastructure\Service\Validation\Constrain\Operator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\ChoiceValidator;

class OperatorValidator extends ChoiceValidator
{
    public function __construct(private CalculatorAdapter $calculatorAdapter)
    {
    }

    public function validate(mixed $value, Constraint $constraint)
    {
        if ($constraint instanceof Operator) {
            $constraint->choices = $this->calculatorAdapter->getSupportedOperations();
        }

        parent::validate($value, $constraint);
    }
}
