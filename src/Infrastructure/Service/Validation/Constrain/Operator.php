<?php

namespace App\Infrastructure\Service\Validation\Constrain;

use App\Infrastructure\Service\Validation\OperatorValidator;
use Symfony\Component\Validator\Constraints\Choice;

#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class Operator extends Choice
{
    public function validatedBy()
    {
        return OperatorValidator::class;
    }
}
