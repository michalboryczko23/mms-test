<?php

namespace App\Infrastructure\Service\Validation;

use App\Infrastructure\Exception\RestApiValidationException;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RestApiValidator
{
    public function __construct(
        private ValidatorInterface $validator,
    ) {
    }

    public function validate(object $validationObject): void
    {
        $errors = $this->validator->validate($validationObject);
        if ($errors->count()) {
            $errorMessage = [];
            /** @var ConstraintViolationInterface $violation */
            foreach ($errors as $violation) {
                $errorMessage[$violation->getPropertyPath()][] = $violation->getMessage();
            }
            throw new RestApiValidationException($errorMessage);
        }
    }
}
