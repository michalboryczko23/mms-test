<?php

namespace App\Infrastructure\Service\ApiResponseStrategy;

use App\Core\Exception\DivisionByZeroException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DividedByZeroExceptionStrategy implements ApiResponseExceptionResolver
{
    /**
     * @param DivisionByZeroException $exception
     */
    public function getResponse(\Throwable $exception): JsonResponse
    {
        return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
    }

    public function supports(\Throwable $exception): bool
    {
        return $exception instanceof DivisionByZeroException;
    }
}
