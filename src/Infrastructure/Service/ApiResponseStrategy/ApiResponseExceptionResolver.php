<?php

namespace App\Infrastructure\Service\ApiResponseStrategy;

use Symfony\Component\HttpFoundation\JsonResponse;

interface ApiResponseExceptionResolver
{
    public function getResponse(\Throwable $exception): JsonResponse;

    public function supports(\Throwable $exception): bool;
}
