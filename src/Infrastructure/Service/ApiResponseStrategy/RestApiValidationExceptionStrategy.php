<?php

namespace App\Infrastructure\Service\ApiResponseStrategy;

use App\Infrastructure\Exception\RestApiValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class RestApiValidationExceptionStrategy implements ApiResponseExceptionResolver
{
    /**
     * @param RestApiValidationException $exception
     */
    public function getResponse(\Throwable $exception): JsonResponse
    {
        return new JsonResponse(
            [
                'message' => $exception->getMessage(),
                'fields' => $exception->getValidationErrors(),
            ],
            Response::HTTP_BAD_REQUEST
        );
    }

    public function supports(\Throwable $exception): bool
    {
        return $exception instanceof RestApiValidationException;
    }
}
