<?php

namespace App\Infrastructure\EventSubscriber;

use App\Infrastructure\Service\ApiResponseStrategy\ApiResponseExceptionResolver;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiEventSubscriber implements EventSubscriberInterface
{
    public function __construct(private iterable $responseStrategies)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        /** @var ApiResponseExceptionResolver $responseStrategy */
        foreach ($this->responseStrategies as $responseStrategy) {
            if ($responseStrategy->supports($exception)) {
                $event->setResponse($responseStrategy->getResponse($exception));

                return;
            }
        }

        $event->setResponse(new JsonResponse([
            'message' => $exception->getMessage(),
        ], Response::HTTP_INTERNAL_SERVER_ERROR));
    }
}
