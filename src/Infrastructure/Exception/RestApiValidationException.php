<?php

namespace App\Infrastructure\Exception;

class RestApiValidationException extends \Exception
{
    public const MESSAGE = 'Validation exception.';

    private array $validationErrors;

    public function __construct(array $validationErrors)
    {
        $this->validationErrors = $validationErrors;

        parent::__construct(self::MESSAGE);
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }
}
