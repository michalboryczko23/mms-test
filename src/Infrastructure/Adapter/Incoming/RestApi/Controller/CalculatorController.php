<?php

namespace App\Infrastructure\Adapter\Incoming\RestApi\Controller;

use App\Core\DTO\CalculatorInputDTO;
use App\Core\Port\Incoming\CalculatorIncomingPort;
use App\Infrastructure\Adapter\Incoming\RestApi\Input\CalculatorInput;
use App\Infrastructure\Service\Validation\RestApiValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CalculatorController
{
    public function __construct(
        private CalculatorIncomingPort $calculatorIncomingPort,
        private SerializerInterface $serializer,
        private RestApiValidator $restApiValidator
    ) {
    }

    #[Route(path: 'api/calculate', methods: [Request::METHOD_POST])]
    public function calculate(Request $request)
    {
        $inputData = $this->getCalculateInputData($request);

        return new JsonResponse([
            'result' => $this->calculatorIncomingPort->calculate(new CalculatorInputDTO($inputData->getA(), $inputData->getB(), $inputData->getOperator())),
        ]);
    }

    #[Route(path: 'api/operations', methods: [Request::METHOD_GET])]
    public function getOperationList()
    {
        return new JsonResponse(['operations' => $this->calculatorIncomingPort->provideSupportedOperations()]);
    }

    private function getCalculateInputData(Request $request): CalculatorInput
    {
        $input = $this->serializer->deserialize($request->getContent(), CalculatorInput::class, 'json');
        $this->restApiValidator->validate($input);

        return $input;
    }
}
