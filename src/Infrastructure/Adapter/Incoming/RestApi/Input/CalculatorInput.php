<?php

namespace App\Infrastructure\Adapter\Incoming\RestApi\Input;

use Symfony\Component\Validator\Constraints\NotBlank;
use App\Infrastructure\Service\Validation\Constrain\Operator;

class CalculatorInput
{
    #[NotBlank]
    private float $a;

    #[NotBlank]
    private float $b;

    #[Operator, NotBlank]
    private string $operator;

    public function setA(float $a): void
    {
        $this->a = $a;
    }

    public function setB(float $b): void
    {
        $this->b = $b;
    }

    public function setOperator(string $operator): void
    {
        $this->operator = $operator;
    }

    public function getA(): float
    {
        return $this->a;
    }

    public function getB(): float
    {
        return $this->b;
    }

    public function getOperator(): string
    {
        return $this->operator;
    }
}
