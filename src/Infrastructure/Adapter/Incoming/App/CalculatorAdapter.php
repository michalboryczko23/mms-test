<?php

namespace App\Infrastructure\Adapter\Incoming\App;

use App\Core\Port\Incoming\CalculatorIncomingPort;

class CalculatorAdapter
{
    public function __construct(private CalculatorIncomingPort $calculatorIncomingPort)
    {
    }

    public function getSupportedOperations(): array
    {
        return $this->calculatorIncomingPort->provideSupportedOperations();
    }
}
