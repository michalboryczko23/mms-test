<?php

namespace App\Core\Port\Incoming;

use App\Core\DTO\CalculatorInputDTO;
use App\Core\Service\CalculatorService;

class CalculatorIncomingPort
{
    public function __construct(private CalculatorService $calculatorService)
    {
    }

    public function calculate(CalculatorInputDTO $inputDTO): float
    {
        return $this->calculatorService->calculate($inputDTO);
    }

    public function provideSupportedOperations(): array
    {
        return $this->calculatorService->getAllSupportedOperations();
    }
}
