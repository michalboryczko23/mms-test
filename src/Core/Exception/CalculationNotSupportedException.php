<?php

namespace App\Core\Exception;

use App\Core\DTO\CalculatorInputDTO;

class CalculationNotSupportedException extends CoreException
{
    public function __construct(CalculatorInputDTO $calculatorInputDTO)
    {
        parent::__construct(
            sprintf(
                'Calculation for equation \'%s %s %s\' is not supported',
                $calculatorInputDTO->getA(),
                $calculatorInputDTO->getOperator(),
                $calculatorInputDTO->getB()
            )
        );
    }
}
