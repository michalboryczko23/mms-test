<?php

namespace App\Core\Exception;

class DivisionByZeroException extends CoreException
{
    public const MESSAGE = 'Cannot be divided by 0';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
