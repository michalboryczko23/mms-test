<?php

namespace App\Core\DTO;

class CalculatorInputDTO
{
    public function __construct(
        private float $a,
        private float $b,
        private string $operator,
    ) {
    }

    public function getA(): float
    {
        return $this->a;
    }

    public function getB(): float
    {
        return $this->b;
    }

    public function getOperator(): string
    {
        return $this->operator;
    }
}
