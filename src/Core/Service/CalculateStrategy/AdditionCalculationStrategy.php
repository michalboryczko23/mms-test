<?php

namespace App\Core\Service\CalculateStrategy;

use App\Core\DTO\CalculatorInputDTO;

class AdditionCalculationStrategy extends BaseCalculationStrategy implements CalculationStrategyInterface
{
    private const OPERATOR = '+';

    public function getOperatorDefinition(): string
    {
        return self::OPERATOR;
    }

    public function calculate(CalculatorInputDTO $inputDTO): float
    {
        return $inputDTO->getA() + $inputDTO->getB();
    }
}
