<?php

namespace App\Core\Service\CalculateStrategy;

use App\Core\DTO\CalculatorInputDTO;

interface CalculationStrategyInterface
{
    public function getOperatorDefinition(): string;

    public function supports(CalculatorInputDTO $inputDTO): bool;

    public function calculate(CalculatorInputDTO $inputDTO): float;
}
