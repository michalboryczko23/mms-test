<?php

namespace App\Core\Service\CalculateStrategy;

use App\Core\DTO\CalculatorInputDTO;
use App\Core\Exception\DivisionByZeroException;

class DivisionCalculationStrategy extends BaseCalculationStrategy implements CalculationStrategyInterface
{
    private const OPERATOR = '/';

    public function getOperatorDefinition(): string
    {
        return self::OPERATOR;
    }

    public function calculate(CalculatorInputDTO $inputDTO): float
    {
        if (0 == $inputDTO->getB()) {
            throw new DivisionByZeroException();
        }

        return $inputDTO->getA() / $inputDTO->getB();
    }
}
