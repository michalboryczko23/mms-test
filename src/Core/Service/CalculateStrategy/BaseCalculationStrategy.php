<?php

namespace App\Core\Service\CalculateStrategy;

use App\Core\DTO\CalculatorInputDTO;

abstract class BaseCalculationStrategy implements CalculationStrategyInterface
{
    public function supports(CalculatorInputDTO $inputDTO): bool
    {
        return $this->getOperatorDefinition() === $inputDTO->getOperator();
    }
}
