<?php

namespace App\Core\Service;

use App\Core\DTO\CalculatorInputDTO;
use App\Core\Exception\CalculationNotSupportedException;
use App\Core\Service\CalculateStrategy\CalculationStrategyInterface;

class CalculatorService
{
    public function __construct(private iterable $calculationStrategies)
    {
    }

    public function calculate(CalculatorInputDTO $input): float
    {
        /** @var CalculationStrategyInterface $calculationStrategy */
        foreach ($this->calculationStrategies as $calculationStrategy) {
            if (!$calculationStrategy->supports($input)) {
                continue;
            }

            return $calculationStrategy->calculate($input);
        }

        throw new CalculationNotSupportedException($input);
    }

    public function getAllSupportedOperations(): array
    {
        $operations = [];
        /** @var CalculationStrategyInterface $calculationStrategy */
        foreach ($this->calculationStrategies as $calculationStrategy) {
            $operations[] = $calculationStrategy->getOperatorDefinition();
        }

        return array_unique($operations);
    }
}
