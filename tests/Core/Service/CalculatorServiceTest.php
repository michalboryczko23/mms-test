<?php

namespace App\Tests\Core\Service;

use App\Core\DTO\CalculatorInputDTO;
use App\Core\Exception\CalculationNotSupportedException;
use App\Core\Service\CalculateStrategy\CalculationStrategyInterface;
use App\Core\Service\CalculatorService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CalculatorServiceTest extends KernelTestCase
{
    private $strategyImplementation;

    private $otherStrategyImplementation;

    private CalculatorService $calculatorService;

    public function setUp(): void
    {
        $this->strategyImplementation = $this->getMockForAbstractClass(CalculationStrategyInterface::class);
        $this->otherStrategyImplementation = $this->getMockForAbstractClass(CalculationStrategyInterface::class);

        $this->calculatorService = new CalculatorService([$this->strategyImplementation, $this->otherStrategyImplementation]);
    }

    public function testCalculateWillThrowExceptionIfCalculationNotSupported(): void
    {
        $input = new CalculatorInputDTO(1, 1, '-');

        $this->strategyImplementation->expects($this->once())
            ->method('supports')
            ->with($input)
            ->willReturn(false);
        $this->otherStrategyImplementation->expects($this->once())
            ->method('supports')
            ->with($input)
            ->willReturn(false);

        $this->strategyImplementation->expects($this->never())
            ->method('calculate');
        $this->otherStrategyImplementation->expects($this->never())
            ->method('calculate');

        $this->expectException(CalculationNotSupportedException::class);

        $this->calculatorService->calculate($input);
    }

    public function testCalculateWillReturnResultFromFirstSupportedStrategyImplementation(): void
    {
        $input = new CalculatorInputDTO(1, 1, '-');
        $expectedResult = 0;

        $this->strategyImplementation->expects($this->once())
            ->method('supports')
            ->with($input)
            ->willReturn(true);
        $this->otherStrategyImplementation->expects($this->never())
            ->method('supports');

        $this->strategyImplementation->expects($this->once())
            ->method('calculate')
            ->with($input)
            ->willReturn((float) $expectedResult);
        $this->otherStrategyImplementation->expects($this->never())
            ->method('calculate');

        $this->assertEquals($expectedResult, $this->calculatorService->calculate($input));
    }

    public function testGetAllSupportedOperations(): void
    {
        $strategyOperator = '+';
        $otherStrategyOperator = '-';
        $expectedResult = [$strategyOperator, $otherStrategyOperator];

        $this->strategyImplementation->expects($this->once())
            ->method('getOperatorDefinition')
            ->willReturn($strategyOperator);
        $this->otherStrategyImplementation->expects($this->once())
            ->method('getOperatorDefinition')
            ->willReturn($otherStrategyOperator);

        $this->assertEquals($expectedResult, $this->calculatorService->getAllSupportedOperations());
    }
}
