<?php

namespace App\Tests\Core\Service\CalculateStrategy;

use App\Core\Service\CalculateStrategy\AdditionCalculationStrategy;
use App\Core\Service\CalculateStrategy\CalculationStrategyInterface;

class AdditionCalculationStrategyTest extends CalculateStrategyTest
{
    protected function calculateTestDataProvider(): array
    {
        return [
            'Test if 2+2=4' => [$this->createInputDTO(2, 2, '+'), 4],
            'Test if 2+(-2)=0' => [$this->createInputDTO(2, -2, '+'), 0],
            'Test if -2+(-2)=-4' => [$this->createInputDTO(-2, -2, '+'), -4],
            'Test if -0.111+(-0.011)=-0.122' => [$this->createInputDTO(-0.111, -0.011, '+'), -0.122],
        ];
    }

    protected function supportsTestDataProvider(): array
    {
        return [
            'Test if support addition' => [$this->createInputDTO(2, 2, '+'), true],
            'Test if support multiply' => [$this->createInputDTO(2, 2, '*'), false],
            'Test if support random chars' => [$this->createInputDTO(2, 2, 'dasfaf'), false],
        ];
    }

    protected function createCalculation(): CalculationStrategyInterface
    {
        return new AdditionCalculationStrategy();
    }
}
