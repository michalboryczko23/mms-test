<?php

namespace App\Tests\Core\Service\CalculateStrategy;

use App\Core\Exception\DivisionByZeroException;
use App\Core\Service\CalculateStrategy\CalculationStrategyInterface;
use App\Core\Service\CalculateStrategy\DivisionCalculationStrategy;

class DivisionCalculationStrategyTest extends CalculateStrategyTest
{
    public function testDivisionByZero(): void
    {
        $this->expectException(DivisionByZeroException::class);

        $this->getCalculation()->calculate($this->createInputDTO(11, 0, '/'));
    }

    protected function calculateTestDataProvider(): array
    {
        return [
            'Test if 2/2=1' => [$this->createInputDTO(2, 2, '/'), 1],
            'Test if 1/2=0.5' => [$this->createInputDTO(1, 2, '/'), 0.5],
            'Test if 0.5/0.1=5' => [$this->createInputDTO(0.5, 0.1, '/'), 5],
        ];
    }

    protected function supportsTestDataProvider(): array
    {
        return [
            'Test if support addition' => [$this->createInputDTO(2, 2, '+'), false],
            'Test if support multiply' => [$this->createInputDTO(2, 2, '*'), false],
            'Test if support division' => [$this->createInputDTO(2, 2, '/'), true],
            'Test if support random chars' => [$this->createInputDTO(2, 2, 'dasfaf'), false],
        ];
    }

    protected function createCalculation(): CalculationStrategyInterface
    {
        return new DivisionCalculationStrategy();
    }
}
