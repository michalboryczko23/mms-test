<?php

namespace App\Tests\Core\Service\CalculateStrategy;

use App\Core\Service\CalculateStrategy\CalculationStrategyInterface;
use App\Core\Service\CalculateStrategy\MultiplicationCalculationStrategy;

class MultiplyCalculationStrategyTest extends CalculateStrategyTest
{
    protected function calculateTestDataProvider(): array
    {
        return [
            'Test if 2*2=4' => [$this->createInputDTO(2, 2, '*'), 4],
            'Test if 2*(-2)=-4' => [$this->createInputDTO(2, -2, '*'), -4],
            'Test if 0.1*0.1=0.01' => [$this->createInputDTO(0.1, 0.1, '*'), 0.01],
        ];
    }

    protected function supportsTestDataProvider(): array
    {
        return [
            'Test if support addition' => [$this->createInputDTO(2, 2, '+'), false],
            'Test if support multiply' => [$this->createInputDTO(2, 2, '*'), true],
            'Test if support random chars' => [$this->createInputDTO(2, 2, 'dasfaf'), false],
        ];
    }

    protected function createCalculation(): CalculationStrategyInterface
    {
        return new MultiplicationCalculationStrategy();
    }
}
