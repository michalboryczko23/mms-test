<?php

namespace App\Tests\Core\Service\CalculateStrategy;

use App\Core\DTO\CalculatorInputDTO;
use App\Core\Service\CalculateStrategy\CalculationStrategyInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class CalculateStrategyTest extends KernelTestCase
{
    private CalculationStrategyInterface $calculation;

    public function setUp(): void
    {
        $this->calculation = $this->createCalculation();
    }

    /**
     * @dataProvider calculateTestDataProvider
     */
    public function testCalculate(CalculatorInputDTO $calculatorInputDTO, float $expectedResult): void
    {
        $this->assertEquals(
            $this->calculation->calculate($calculatorInputDTO),
            $expectedResult
        );
    }

    /**
     * @dataProvider supportsTestDataProvider
     */
    public function testSupports(CalculatorInputDTO $calculatorInputDTO, bool $expectedResult): void
    {
        $this->assertEquals(
            $this->calculation->supports($calculatorInputDTO),
            $expectedResult
        );
    }

    abstract protected function calculateTestDataProvider(): array;

    abstract protected function supportsTestDataProvider(): array;

    abstract protected function createCalculation(): CalculationStrategyInterface;

    protected function createInputDTO(float $a, float $b, string $operator): CalculatorInputDTO
    {
        return new CalculatorInputDTO($a, $b, $operator);
    }

    protected function getCalculation(): CalculationStrategyInterface
    {
        return $this->calculation;
    }
}
