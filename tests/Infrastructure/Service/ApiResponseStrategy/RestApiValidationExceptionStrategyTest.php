<?php

namespace App\Tests\Infrastructure\Service\ApiResponseStrategy;

use App\Infrastructure\Exception\RestApiValidationException;
use App\Infrastructure\Service\ApiResponseStrategy\ApiResponseExceptionResolver;
use App\Infrastructure\Service\ApiResponseStrategy\RestApiValidationExceptionStrategy;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class RestApiValidationExceptionStrategyTest extends BaseApiResponseExceptionResolverTest
{
    protected function createInstanceOfStrategy(): ApiResponseExceptionResolver
    {
        return new RestApiValidationExceptionStrategy();
    }

    protected function getResponseTestDataProvider(): array
    {
        $validationErrors = ['field' => 'some errors'];

        return [
            [new RestApiValidationException($validationErrors), new JsonResponse(['message' => RestApiValidationException::MESSAGE, 'fields' => $validationErrors], Response::HTTP_BAD_REQUEST)],
        ];
    }

    protected function supportsTestDataProvider(): array
    {
        return [
            'base exception will return false' => [new \Exception(), false],
            'RestApiValidationException will return true' => [new RestApiValidationException([]), true],
        ];
    }
}
