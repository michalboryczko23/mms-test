<?php

namespace App\Tests\Infrastructure\Service\ApiResponseStrategy;

use App\Infrastructure\Service\ApiResponseStrategy\ApiResponseExceptionResolver;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class BaseApiResponseExceptionResolverTest extends KernelTestCase
{
    private ApiResponseExceptionResolver $resolver;

    public function setUp(): void
    {
        $this->resolver = $this->createInstanceOfStrategy();
    }

    /** @dataProvider supportsTestDataProvider */
    public function testSupports(\Throwable $input, bool $result): void
    {
        $this->assertEquals(
            $this->resolver->supports($input),
            $result
        );
    }

    /** @dataProvider getResponseTestDataProvider */
    public function testGetResponse(\Throwable $input, JsonResponse $result): void
    {
        $this->assertEquals(
            $this->resolver->getResponse($input),
            $result
        );
    }

    abstract protected function createInstanceOfStrategy(): ApiResponseExceptionResolver;

    abstract protected function getResponseTestDataProvider(): array;

    abstract protected function supportsTestDataProvider(): array;
}
