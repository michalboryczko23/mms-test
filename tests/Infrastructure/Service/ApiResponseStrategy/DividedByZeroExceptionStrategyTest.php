<?php

namespace App\Tests\Infrastructure\Service\ApiResponseStrategy;

use App\Core\Exception\DivisionByZeroException;
use App\Infrastructure\Service\ApiResponseStrategy\ApiResponseExceptionResolver;
use App\Infrastructure\Service\ApiResponseStrategy\DividedByZeroExceptionStrategy;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DividedByZeroExceptionStrategyTest extends BaseApiResponseExceptionResolverTest
{
    protected function createInstanceOfStrategy(): ApiResponseExceptionResolver
    {
        return new DividedByZeroExceptionStrategy();
    }

    protected function getResponseTestDataProvider(): array
    {
        return [
            [new DivisionByZeroException(), new JsonResponse(['message' => DivisionByZeroException::MESSAGE], Response::HTTP_BAD_REQUEST)],
        ];
    }

    protected function supportsTestDataProvider(): array
    {
        return [
            'base exception will return false' => [new \Exception(), false],
            'DividedByZeroException will return true' => [new DivisionByZeroException(), true],
        ];
    }
}
