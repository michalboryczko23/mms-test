<?php

namespace App\Tests\Infrastructure\Service\Validation;

use App\Infrastructure\Exception\RestApiValidationException;
use App\Infrastructure\Service\Validation\RestApiValidator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RestApiValidatorTest extends KernelTestCase
{
    private $validator;

    private RestApiValidator $apiValidator;

    public function setUp(): void
    {
        $this->validator = $this->getMockForAbstractClass(ValidatorInterface::class);
        $this->apiValidator = new RestApiValidator($this->validator);
    }

    public function testValidateWillThrowErrorInvalidObject(): void
    {
        $errors = $this->getMockForAbstractClass(ConstraintViolationListInterface::class);
        $errors->method('count')
            ->willReturn(2);
        $this->validator->method('validate')
            ->with(new \stdClass())
            ->willReturn($errors);
        $this->expectException(RestApiValidationException::class);

        $this->apiValidator->validate(new \stdClass());
    }
}
