<?php

namespace App\Tests\Infrastructure\EventSubscriber;

use App\Infrastructure\EventSubscriber\ApiEventSubscriber;
use App\Infrastructure\Service\ApiResponseStrategy\ApiResponseExceptionResolver;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class ApiEventSubscriberTest extends KernelTestCase
{
    private $strategyImplementation;

    private $otherStrategyImplementation;

    private ApiEventSubscriber $apiEventSubscriber;

    public function setUp(): void
    {
        $this->strategyImplementation = $this->getMockForAbstractClass(ApiResponseExceptionResolver::class);
        $this->otherStrategyImplementation = $this->getMockForAbstractClass(ApiResponseExceptionResolver::class);

        $this->apiEventSubscriber = new ApiEventSubscriber([$this->strategyImplementation, $this->otherStrategyImplementation]);
    }

    public function testOnKernelRequestWillSetDefaultResponseIfNotSupportedStrategy(): void
    {
        $exceptionMessage = 'exception message';
        $expectedResponse = new JsonResponse(['message' => $exceptionMessage], Response::HTTP_INTERNAL_SERVER_ERROR);
        $exception = new \Exception($exceptionMessage);
        $event = $this->createEvent($exception);

        $this->strategyImplementation->expects($this->once())
            ->method('supports')
            ->with($exception)
            ->willReturn(false);
        $this->otherStrategyImplementation->expects($this->once())
            ->method('supports')
            ->with($exception)
            ->willReturn(false);

        $this->strategyImplementation->expects($this->never())
            ->method('getResponse');
        $this->otherStrategyImplementation->expects($this->never())
            ->method('getResponse');

        $this->apiEventSubscriber->onKernelException($event);
        $this->assertEquals($event->getResponse(), $expectedResponse);
    }

    public function testOnKernelRequestWillUseFirstStrategy(): void
    {
        $exceptionMessage = 'exception message';
        $exception = new \Exception($exceptionMessage);
        $expectedResponse = new JsonResponse(['message' => $exceptionMessage], Response::HTTP_INTERNAL_SERVER_ERROR);
        $event = $this->createEvent($exception);

        $this->strategyImplementation->expects($this->once())
            ->method('supports')
            ->with($exception)
            ->willReturn(true);
        $this->otherStrategyImplementation->expects($this->never())
            ->method('supports');

        $this->strategyImplementation->expects($this->once())
            ->method('getResponse')
            ->with($exception)
            ->willReturn($expectedResponse);
        $this->otherStrategyImplementation->expects($this->never())
            ->method('getResponse');

        $this->apiEventSubscriber->onKernelException($event);
        $this->assertEquals($event->getResponse(), $expectedResponse);
    }

    private function createEvent(\Throwable $throwable): ExceptionEvent
    {
        $kernel = $this->getMockForAbstractClass(HttpKernelInterface::class);
        $request = $this->createMock(Request::class);

        return new ExceptionEvent($kernel, $request, 1, $throwable);
    }
}
